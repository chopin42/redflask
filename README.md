# A simple reddit clone for Flask

This simple reddit clone is going to implement the following things:

* Communities
* Users and accoutns
* Posts
* Upvotes and downvotes

The comments are not going to be implemented for know to keep the code clear.

## Install

### Dependencies

* Python3
* Flask
* Flask-login
* SqlAlchemy

### Installing

No packages for the moments, so execute the following command in the folder:

```shell
python3 main.py
```

Now you can go on http://localhost:5000/register to get the app.

## License

The LICENSE file is not yet uploaded but the license will be GNU-GPL-V3.

The maintainer of this project is chopin42 (snowcode)