from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, logout_user, login_required, current_user
import hashlib

app = Flask(__name__)

# The env values
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['SECRET_KEY'] = 'verysecretindeed'

# Import the db
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)

# The class db
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Text, unique=True)
    password = db.Column(db.Text)
    upvotes = db.Column(db.Text)
    downvotes = db.Column(db.Text)

    def __init__(self, username, password, upvotes, downvotes):
        self.username = username
        self.password = password
        self.upvotes = upvotes
        self.downvotes = downvotes
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    link = db.Column(db.Text)
    score = db.Column(db.Integer)
    community = db.Column(db.Text)
    by = db.Column(db.Text)

    def __init__(self, link, community, by, score):
        self.link = link
        self.community = community
        self.score = score
        self.by = by
db.create_all()

# The callback function
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

# ------------------------------------------------------------------------------

@app.route('/login')
def loginpage():
    return render_template('login.html')

@app.route('/login/post', methods=['POST'])
def login():
    username = request.form['username']
    password = request.form['password']

    # Hashthemall
    #username = hashlib.sha224(username.encode('utf-8')).hexdigest()
    password = hashlib.sha224(password.encode('utf-8')).hexdigest()

    # Check if it exists
    try:
        user = User.query.filter_by(username=username, password=password).first_or_404()
        login_user(user)
        return 'You are now logged in'
    except:
         return 'Invalid login'

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return 'You are now logged out!'

@app.route('/home')
@login_required
def home():
    return 'This page is protected. You are ' + current_user.username

@app.route('/register')
def registerpage():
    return render_template('register.html')

@app.route('/register/post', methods=['POST'])
def register():
    username = request.form['username']
    password = request.form['password']

    # Hashthemall
    #username = hashlib.sha224(username.encode('utf-8')).hexdigest()
    password = hashlib.sha224(password.encode('utf-8')).hexdigest()

    #Send them to db
    new = User(username, password, '[]', '[]')
    db.session.add(new)
    db.session.commit()

    # Return to login
    return redirect('/login')

@app.route('/protected')
@login_required
def protected():
    return 'This is a secret page, you are logged as ' + current_user.username

# ------------------------------------------------------------------------------

@app.route('/submit')
@login_required
def submit():
    return render_template('submit.html')

@app.route('/')
def homepage():
    home = Post.query.all()
    return render_template('index.html', home=home)

@app.route('/post', methods=['POST'])
def post():
    link = request.form['link']
    community = request.form['community']

    # Add a new row
    newPost = Post(link, community, current_user.username, 0)
    db.session.add(newPost)
    db.session.commit()
    return redirect('/')

import json
@app.route('/up/<int:id>')
def upvote(id):
    item = Post.query.filter_by(id=id).first_or_404()

    # Get the username
    username = current_user.username

    # Get the list
    user = User.query.filter_by(username=username).first_or_404()
    upvotes = json.loads(user.upvotes)
    downvotes = json.loads(user.downvotes)

    # If the post is in the upvotes, undo
    if item.id in upvotes:
        # Remove from the upvotes
        upvotes.remove(item.id)
        user.upvotes = json.dumps(upvotes)

        # Cancel
        item.score = item.score - 1
        db.session.commit()

        # Redirect to homepage
        return redirect('/')

    # If the post is in the list downvotes, increment of two
    elif item.id in downvotes:
        # Remove from the list
        downvotes.remove(item.id)
        user.downvotes = json.dumps(downvotes)

        # Add to the list
        upvotes.append(item.id)
        user.upvotes = json.dumps(upvotes)

        #Increment by two to the item
        item.score = item.score + 2
        db.session.commit()

        # Redirect to homepage
        return redirect('/')

    # If the element is not in any list
    else:
        # Append the id into the upvotes
        upvotes.append(item.id)
        user.upvotes = json.dumps(upvotes)

        # Increment one to the item
        item.score = item.score + 1
        db.session.commit()

        # Redirect to homepage
        return redirect('/')

@app.route('/down/<int:id>')
def downvote(id):
    item = Post.query.filter_by(id=id).first_or_404()

    # Get the username
    username = current_user.username

    # Get the list
    user = User.query.filter_by(username=username).first_or_404()
    upvotes = json.loads(user.upvotes)
    downvotes = json.loads(user.downvotes)

    # If the post is in the downvotes
    if item.id in downvotes:
        # Remove from the downvotes
        downvotes.remove(item.id)
        user.downvotes = json.dumps(downvotes)

        # Increment one to the item
        item.score = item.score + 1
        db.session.commit()

        # Redirect to homepage
        return redirect('/')

    # If the post is in the upvotes
    elif item.id in upvotes:
        # Remove from upvotes
        upvotes.remove(item.id)
        user.upvotes = json.dumps(upvotes)

        # Add to downvotes
        downvotes.append(item.id)
        user.downvotes = json.dumps(downvotes)

        # Decrease of 2
        item.score = item.score - 2
        db.session.commit()

        # Return the homepage
        return redirect('/')

    #If is in no lists
    else:
        # Append the id into the list
        downvotes.append(item.id)
        user.downvotes = json.dumps(downvotes)

        # Increment one to the item
        item.score = item.score - 1
        db.session.commit()

        # Redirect to homepage
        return redirect('/')

@app.route('/r/<community>')
def communityUrl(community):
    home = Post.query.filter_by(community=community)
    return render_template('index.html', home=home)

@app.route('/u/<user>')
def userUrl(user):
    home = Post.query.filter_by(by=user)
    return render_template('index.html', home=home)

if __name__ == '__main__':
    app.run(debug=True)
